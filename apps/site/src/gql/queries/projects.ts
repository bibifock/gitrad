import { gql } from '@apollo/client';

export type Project = {
  avatarUrl?: string;
  description?: string;
  fullPath: string;
  name: string;
  path: string;
  webUrl: string;
  visibility: string;
};

export const projectFragment = `
 avatarUrl
 description
 fullPath
 name
 path
 webUrl
 visibility
`;

export type QueryProjectsResponse = {
  projects: Project[];
};

export const QUERY_GET_PROJECTS = gql`
  query getProjects {
    projects {
      ${projectFragment}
    }
  }
`;
