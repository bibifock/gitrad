import { gql } from '@apollo/client';

import { projectFragment } from './projects';

import type { Project } from './projects';

type User = {
  id: number;
  avatarUrl: string;
  username: string;
  name: string;
  webUrl: string;
};

const userFragment = `
  id
  avatarUrl
  username
  name
  webUrl
`;

//type Discussion = {
//id: number;
//resolved: boolean;
//};

export type Label = {
  title: string;
  color: string;
  textColor: string;
};

export type Request = {
  id: number;
  iid: number;
  title: string;
  description: string;
  webUrl: string;
  state: string;
  detailedMergeStatus: string;
  approvedBy: User[];
  //discussions: Discussion[];
  author: User;
  labels: Label[];
  //reviewers: User[];
};

const requestFragment = `
  id
  iid
  title
  description
  webUrl
  state
  detailedMergeStatus
  approvedBy  {
    ${userFragment}
  }
  author  {
    ${userFragment}
  }
  labels { color title textColor }
`;

export type RequestByState = {
  state: string;
  items: Request[];
};

export type QueryProjectResponse = {
  project: Project;
  projectRequests: RequestByState;
};

export const QUERY_GET_PROJECT = gql`
  query getProjectDetails($fullpath: String!) {
    project(path: $fullpath) {
      ${projectFragment}
    }
    projectRequests(path: $fullpath) {
      state
      items {
        ${requestFragment}
      }
    }
  }
`;
