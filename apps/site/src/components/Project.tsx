import { LockClosedIcon } from '@heroicons/react/24/solid';
import { Avatar, Badge } from 'flowbite-react';
import { FC, ComponentProps } from 'react';

import type { Project as ProjectType } from '@/gql/queries/projects';

export const Project = ({
  description,
  webUrl,
  name,
  avatarUrl,
  visibility
}: ProjectType) => (
  <div className="flex items-center space-x-4 w-full">
    <div className="shrink-0">
      <Avatar
        img={avatarUrl}
        placeholderInitials={name.replace(/(\b\w)\w*/g, '$1').toUpperCase()}
        rounded
        bordered
        color="light"
      />
    </div>
    <a
      href={webUrl}
      target="_blank"
      rel="noreferrer noopener"
      className="min-w-0 flex-1"
    >
      <p className="truncate text-sm font-medium">{name}</p>
      <p className="truncate text-sm text-gray-500 dark:text-gray-400">
        {description}
      </p>
    </a>
    {visibility === 'private' ? (
      <Badge color="gray" icon={LockClosedIcon as FC<ComponentProps<'svg'>>} />
    ) : (
      <></>
    )}
  </div>
);
