'use client';
import { MagnifyingGlassIcon } from '@heroicons/react/24/solid';
import { ListGroup, TextInput } from 'flowbite-react';
import { FC, ComponentProps, useState, useEffect, ChangeEvent } from 'react';

import type { Project as ProjectType } from '@/gql/queries/projects';

import { ProjectItem } from './ProjectItem';

type SearchInputProps = {
  onChange: (value: string) => void;
  placeholder?: string;
};

const SearchInput = ({
  onChange,
  placeholder = 'search...'
}: SearchInputProps) => {
  const [value, setValue] = useState<string>();
  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setValue(e.target.value);
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      onChange(value ?? '');
    }, 200);
    return () => clearTimeout(timer);
  }, [value, onChange]);

  return (
    <TextInput
      type="text"
      icon={MagnifyingGlassIcon as FC<ComponentProps<'svg'>>}
      placeholder={placeholder}
      sizing="sm"
      onChange={handleChange}
    />
  );
};

export const Projects = ({ items }: { items: ProjectType[] }) => {
  const [filter, setFilter] = useState<string>('');

  const displayItems = filter
    ? items.filter((p) => p.name.includes(filter))
    : items;

  return (
    <div className="flex flex-col gap-3">
      <SearchInput onChange={setFilter} placeholder="gitlab-org/gitlab" />
      <ListGroup className=" overflow-hidden">
        {displayItems.length > 0 ? (
          displayItems.map((project) => (
            <ProjectItem key={`project-${project.fullPath}`} {...project} />
          ))
        ) : (
          <ListGroup.Item
            href={`/projects/${filter}`}
            className="odd:bg-white even:bg-gray-50"
          >
            <div className="flex items-center space-x-4 w-full">
              <div className="min-w-0 flex-1">
                <p className="truncate text-sm font-medium">{filter}</p>
                <p className="truncate text-sm text-gray-500 dark:text-gray-400">
                  display this project
                </p>
              </div>
            </div>
          </ListGroup.Item>
        )}
      </ListGroup>
    </div>
  );
};
