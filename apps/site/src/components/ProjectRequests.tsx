import { Avatar } from 'flowbite-react';

import type { Label, RequestByState, Request } from '@/gql/queries/project';

const Label = ({ textColor, color, title }: Label) => {
  return (
    <small
      className="text-xs font-semibold border inline-block p-1 rounded last:mr-0 mr-1 bg-light"
      style={{
        color: textColor,
        borderColor: color,
        backgroundColor: color
      }}
    >
      {title}
    </small>
  );
};

const Request = ({ title, author, webUrl, iid, labels }: Request) => {
  return (
    <div className="rounded border flex flex-col p-2 hover:border-blue-500 transition-all">
      <div className="flex items-start gap-1">
        <div className="flex shrink-0">
          <Avatar
            img={author.avatarUrl}
            placeholderInitials={author.username
              .replace(/(\b\w)\w*/g, '$1')
              .toUpperCase()}
            rounded
            bordered
            color="light"
          />
        </div>
        <div className="flex flex-col items-start gap-2 text-sm font-normal text-gray-600 dark:text-gray-400 group-hover:text-gray-300 grow whitespace-nowrap overflow-hidden">
          <span className="text-ellipsis overflow-hidden">{title}</span>
          <p className="flex flex-wrap gap-1">
            {labels.map((label) => (
              <Label {...label} key={`request-${iid}-${label.title}`} />
            ))}
          </p>
        </div>
        <a
          href={webUrl}
          target="_blank"
          rel="noreferrer noopener"
          className="text-xs text-amber-500 hover:underline"
        >
          #{iid}
        </a>
      </div>
    </div>
  );
};

const State = ({ state, items }: RequestByState) => {
  return (
    <div>
      <h4>{state}</h4>
      <div className="flex flex-col gap-1">
        {items.map((item) => (
          <Request {...item} key={`request-${item.id}`} />
        ))}
      </div>
    </div>
  );
};

export const ProjectRequests = ({ items }: { items: RequestByState[] }) => {
  return (
    <div className="flex gap-3">
      {items.map((item) => (
        <State {...item} key={`state-${item.state}`} />
      ))}
    </div>
  );
};
