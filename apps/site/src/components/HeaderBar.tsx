import { Navbar } from 'flowbite-react';

export const HeaderBar = () => (
  <Navbar fluid={true} rounded={true}>
    <Navbar.Brand href="/">
      <img
        src="https://flowbite.com/docs/images/logo.svg"
        className="mr-3 h-6 sm:h-9"
        alt="GitRad"
      />
      <span className="self-center whitespace-nowrap text-xl font-semibold dark:text-white">
        Flowbite
      </span>
    </Navbar.Brand>
  </Navbar>
);
