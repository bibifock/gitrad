'use client';
import { useQuery } from '@apollo/client';

import { Project } from '@/components/Project';
import { ProjectRequests } from '@/components/ProjectRequests';
import { QUERY_GET_PROJECT } from '@/gql/queries/project';
import type { QueryProjectResponse } from '@/gql/queries/project';

const PageContent = ({ fullpath }: { fullpath: string }) => {
  const { loading, data, error } = useQuery<QueryProjectResponse>(
    QUERY_GET_PROJECT,
    { variables: { fullpath } }
  );

  if (loading) {
    return <div>loading...</div>;
  }

  if (error || data === undefined) {
    return <div>Ooops something went wrong...</div>;
  }

  const { project, projectRequests: requests } = data;
  return (
    <>
      <Project {...project} />
      <ProjectRequests items={requests} />
    </>
  );
};

type PageProps = {
  params: { slug: string[] };
};

const Page = ({ params }: PageProps) => {
  const fullpath = params.slug.join('/');

  return (
    <main>
      <PageContent fullpath={fullpath} />
    </main>
  );
};

export default Page;
