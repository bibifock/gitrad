'use client';
import { useQuery } from '@apollo/client';

import { Projects } from '@/components/Projects';
import { QUERY_GET_PROJECTS } from '@/gql/queries/projects';
import type { QueryProjectsResponse } from '@/gql/queries/projects';

const HomeContent = () => {
  const { loading, data, error } =
    useQuery<QueryProjectsResponse>(QUERY_GET_PROJECTS);
  if (loading) {
    return <div>loading...</div>;
  }

  if (error || data === undefined) {
    return <div>Ooops something went wrong...</div>;
  }

  return <Projects items={data.projects} />;
};

const Home = () => (
  <main>
    <HomeContent />
  </main>
);

export default Home;
