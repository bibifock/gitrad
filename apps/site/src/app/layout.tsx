'use client';

import { Inter } from 'next/font/google';

import { HeaderBar } from '@/components/HeaderBar';
import { APIProvider } from '@/gql/APIProvider';

import './globals.css';

const inter = Inter({ subsets: ['latin'] });

export default function RootLayout({
  children
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <APIProvider>
          <HeaderBar />
          <div className="p-2">{children}</div>
        </APIProvider>
      </body>
    </html>
  );
}
