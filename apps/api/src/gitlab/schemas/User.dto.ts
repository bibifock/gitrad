import { ObjectType, Field, ID } from '@nestjs/graphql';

@ObjectType()
export class User {
  @Field(() => ID)
  id: number;
  @Field()
  avatarUrl: string;
  @Field()
  username: string;
  @Field()
  name: string;
  @Field()
  webUrl: string;
}

export const userGQL = `
  id
  avatarUrl
  username
  name
  webUrl
`;
