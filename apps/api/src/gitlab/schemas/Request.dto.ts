import { ObjectType, Field, ID } from '@nestjs/graphql';

import { User, userGQL } from './User.dto';

//@ObjectType()
//class Discussion {
//@Field(() => ID)
//id: number;
//@Field()
//resolved: boolean;
//}

@ObjectType()
export class Label {
  @Field(() => ID)
  title: string;
  @Field()
  color: string;
  @Field()
  textColor: string;
}

@ObjectType()
export class Request {
  @Field(() => ID)
  id: string;
  @Field()
  iid: number;
  @Field()
  title: string;
  @Field()
  description: string;
  @Field()
  webUrl: string;
  @Field()
  state: string;
  @Field()
  detailedMergeStatus: string;
  @Field(() => [User], { nullable: 'itemsAndList' })
  approvedBy?: User[];
  //@Field(() => [Discussion], { nullable: 'itemsAndList' })
  //discussions: Discussion[];
  @Field(() => User)
  author: User;
  //@Field(() => [User], { nullable: 'itemsAndList' })
  //reviewers?: User[];
  @Field(() => [Label], { nullable: 'itemsAndList' })
  labels?: Label[];
}

@ObjectType()
export class RequestByState {
  @Field(() => ID)
  state: string;
  @Field(() => [Request])
  items: Request[];
}

export const requestGQL = `
  id
  iid
  title
  description
  webUrl
  state
  detailedMergeStatus
  approvedBy { nodes { ${userGQL} } }
  author { ${userGQL} }
  labels {
    nodes { color title textColor }
  }
`;
