import { ObjectType, Field, ID } from '@nestjs/graphql';

@ObjectType()
export class Project {
  @Field({ nullable: true })
  avatarUrl: string;
  @Field({ nullable: true })
  description?: string;
  @Field(() => ID)
  fullPath: string;
  @Field()
  name: string;
  @Field()
  path: string;
  @Field()
  webUrl: string;
  @Field()
  visibility: string;
}

export const projectGQL = `
  avatarUrl
  description
  fullPath
  name
  path
  webUrl
  visibility
`;
