import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { groupBy } from 'lodash';

import { Project, projectGQL } from './schemas/Project.dto';
import {
  Label,
  Request,
  RequestByState,
  requestGQL
} from './schemas/Request.dto';
import { User } from './schemas/User.dto';

@Injectable()
export class GitlabService {
  constructor(private configService: ConfigService) {}

  callAPI<Response, Variables = undefined>(
    query: string,
    variables?: Variables
  ): Promise<Response> {
    const token = this.configService.get('gitlabToken');
    const apiURL = this.configService.get('gitlabUrl');

    return fetch(apiURL, {
      method: 'POST',
      headers: {
        Authorization: `bearer ${token}`,
        ['Content-Type']: 'application/json'
      },
      body: JSON.stringify({ query, variables })
    })
      .then((response) => response.json())
      .catch((e) => {
        // eslint-disable-next-line no-console
        console.error('FAILED', e);
        throw new Error('Failed to make this gitlab request');
      })
      .then((res) => {
        // TODO need to improve this error management
        if (res.errors) {
          throw res.errors;
        }
        return res.data as Response;
      });
  }

  async getProjects(): Promise<Project[]> {
    const results = await this.callAPI<{
      currentUser: {
        projectMemberships: {
          nodes: Array<{
            id: number;
            project: Project;
          }>;
        };
      };
    }>(`
      query {
        currentUser {
          projectMemberships {
            nodes {
              project {
                ${projectGQL}
              }
            }
          }
        }
      }
    `);

    return results.currentUser.projectMemberships.nodes.map((v) => v.project);
  }

  async getProject(fullPath: string): Promise<Project> {
    const data = await this.callAPI<
      {
        project: Omit<Project, 'requests'> & {
          mergeRequests: {
            nodes: Request[];
          };
        };
      },
      { fullPath: string }
    >(
      `
      query($fullPath: ID!) {
        project(fullPath: $fullPath) {
          ${projectGQL}
        }
      }
      `,
      { fullPath }
    );

    return data.project;
  }

  async getProjectRequests(fullPath: string): Promise<RequestByState[]> {
    const data = await this.callAPI<
      {
        project: {
          mergeRequests: {
            nodes: Array<
              Omit<Request, 'discussions' | 'approvedBy' | 'labels'> & {
                approvedBy: { nodes: User[] };
                labels: { nodes: Label[] };
                //discussions: { nodes: Discussion[] };
              }
            >;
          };
        };
      },
      { fullPath: string }
    >(
      `
        query($fullPath: ID!) {
          project(fullPath: $fullPath) {
            mergeRequests {
              nodes { ${requestGQL} }
            }
          }
        }
      `,
      {
        fullPath
      }
    );
    //console.log(data.project.mergeRequests.nodes[0]);

    const groups = groupBy(
      data.project.mergeRequests.nodes.map(
        ({ author, labels, approvedBy, ...p }) => ({
          ...p,
          author: {
            ...author,
            avatarUrl: author.avatarUrl.replace(/^\//, 'https://gitlab.com/')
          },
          approvedBy: approvedBy.nodes,
          labels: labels.nodes
          //discussions: p.discussions.nodes
        })
      ),
      'state'
    );

    return Object.keys(groups).map((state) => ({
      state,
      items: groups[state]
    }));
  }
}
