export const loader = () => ({
  gitlabToken: process.env.GITLAB_PERSONAL_TOKEN,
  gitlabUrl: 'https://gitlab.com/api/graphql'
});
