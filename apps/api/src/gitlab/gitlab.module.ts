import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { loader } from './config/loader';
import { GitlabService } from './gitlab.service';
import { ProjectsResolver } from './projects.resolver';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [loader]
    })
  ],
  providers: [ProjectsResolver, GitlabService]
})
export class GitlabModule {}
