import { Args, Query, Resolver } from '@nestjs/graphql';

import { GitlabService } from './gitlab.service';
import { Project } from './schemas/Project.dto';
import { RequestByState } from './schemas/Request.dto';

@Resolver(() => Project)
export class ProjectsResolver {
  constructor(private readonly gitlabService: GitlabService) {}

  @Query(() => [Project])
  projects(): Promise<Project[]> {
    return this.gitlabService.getProjects();
  }

  @Query(() => Project)
  project(
    @Args('path', { type: () => String }) path: string
  ): Promise<Project> {
    return this.gitlabService.getProject(path);
  }

  @Query(() => [RequestByState])
  projectRequests(
    @Args('path', { type: () => String }) path: string
  ): Promise<RequestByState[]> {
    return this.gitlabService.getProjectRequests(path);
  }
}
